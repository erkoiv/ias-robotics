import cv2

import math
import threading
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation

from cv_bridge import CvBridge, CvBridgeError

"""
@author: Erik KOIV
@contact: koiverik@gmail.com
"""

#This class is used to define upper and lower limits for the hsv values for colour component of the image
class ColourBounds:
    def __init__(self, rgb):
        hsv = cv2.cvtColor(np.uint8([[[rgb[2], rgb[1], rgb[0]]]]), cv2.COLOR_BGR2HSV).flatten() #to bgr, then to hsv

        var = 179 #base value used for h component in hsv bounds
        lower = [hsv[0] - 10]
        upper = [hsv[0] + 10]

        if lower[0] < 0:
            lower.append(var + lower[0])
            upper.append(var)
            lower[0] = 0
        elif upper[0] > var:
            lower.append(0)
            upper.append(upper[0] - var)
            upper[0] = var

        self.lower = [np.array([h, 30, 30]) for h in lower]
        self.upper = [np.array([h, 255, 255]) for h in upper]

class Plot:
    def __init__(self):
        plt.style.use('classic')
        self.x_values = []
        self.y_values = []

    def plot(self):
        plt.ylim((-500, 500))
        plt.plot(self.x_values, self.y_values)
        plt.savefig('/home/erik/catkin_ws/src/ucat_simulator-master/tut_arrows/scripts/plot.png')
        plt.cla()


class Movement:
    def __init__(self):
        #PD control multipliers set separately for all degrees of movement freedom
        self.yawConsts = [0.03, 0.01] #Kp, Kd
        self.depthConsts = [0.03, 0.01]
        self.surgeConsts = [0.03, 0.01]

        #variables to save data from old iterations
        self.previousDepthErr = 0
        self.previousSurgeErr = 0
        self.previousYawErr = 0
        self.previousDepthForce = 0
        self.previousSurgeForce = 0
        self.previousYawForce = 0
        self.yawTrackingError = 0
        self.depthTrackingError = 0
        self.surgeTrackingError = 0

        #empty variables to be filled by parent (detection) class
        self.radius = 0 #radius of detected ball
        self.X, self.Y = (0,0) #coordinates of detected ball center
        self.desiredX, self.desiredY = (0,0) #desired coordinates in the center of image, set as variables to accommodate resolution change
        self.desiredR = 0 #radius of 'target reticule'
        self.plot = Plot()
    
    '''
    Method to calculate the errors for yaw, depth, and surge all at once on every iteration so that all are updated every iteration.
    'Var' is the reduction coefficient for the pixel values obtained from the camera. This is meant to work similar to gear reduction where a large input value
    is converted to a more suitable value by keeping an overall ratio.

    Currently, after the robot has reached a position where the target and detection radius do not differ more than 30px, which is half of the size of the target reticule, 
    the reduction is increased and moving closer to the ball is stopped, so that less force is applied for small corrections to position.
    '''
    def calcErr(self):
        var = 10
        self.surgeTrackingError= (self.desiredR - self.radius)/var 
        self.yawTrackingError = (self.desiredX - self.X)/var 
        self.depthTrackingError = (self.desiredY - self.Y)/var 
        if(self.surgeTrackingError<3 and self.yawTrackingError<1 and self.depthTrackingError<1.5):
            var = var*2
            self.surgeTrackingError = 0
            self.yawTrackingError = (self.desiredX - self.X)/(var)
            self.depthTrackingError = (self.desiredY - self.Y)/(var)
        #self.plot.y_values.append(self.depthTrackingError)


    '''
    Three methods are written, one for every degree of freedom used for positioning. Separate methods were chosen for easier implementation.
    Ideally a single method with necessary arguments passed in would be used for the same purpose to keep code clean.
    '''
    def yaw(self, dt):
        self.calcErr() #check all errors once more

        #Calculating PD control values
        deltaError = self.previousYawErr - self.yawTrackingError
        derivativeControl = self.yawConsts[1]*(deltaError/dt)
        proportionalControl = self.yawConsts[0]*self.yawTrackingError
        
        #Calculating forces if error larger than margin
        if(abs(self.yawTrackingError)>1):
            if (self.previousYawForce == 0):
                yawF = proportionalControl + derivativeControl
                self.previousYawForce = yawF
                self.increment = yawF/10
            else:
                if(abs(self.previousYawForce>self.increment)):
                    yawF = self.previousYawForce - self.increment
                    self.previousYawForce = yawF
                else:
                    yawF = 0
                    self.previousYawForce = yawF
        else:
            yawF = 0
            self.previousYawForce = yawF
        self.previousYawErr = self.yawTrackingError
        return yawF
    
    def depth(self, dt):
        self.calcErr()

        deltaError = self.previousDepthErr - self.depthTrackingError
        derivativeControl = self.depthConsts[1]*(deltaError/dt)
        proportionalControl = self.depthConsts[0]*self.depthTrackingError

        if(abs(self.depthTrackingError)>1.5):
            if (self.previousDepthForce == 0):
                depthF = proportionalControl + derivativeControl
                self.previousDepthForce = depthF
                self.increment = depthF/10
            else:
                if(abs(self.previousDepthForce>self.increment)):
                    depthF = self.previousDepthForce - self.increment
                    self.previousDepthForce = depthF
                else:
                    depthF = 0
                    self.previousDepthForce = 0
        else:
            depthF = 0
            self.previousDepthForce = 0
        self.previousDepthErr = self.depthTrackingError
        return depthF
    
    def surge(self, dt):
        self.calcErr()
        
        deltaError = self.previousSurgeErr - self.surgeTrackingError
        derivativeControl = self.surgeConsts[1]*(deltaError/dt)
        proportionalControl = self.surgeConsts[0]*self.surgeTrackingError
        
        if(self.surgeTrackingError>3):
            if (self.previousSurgeForce == 0):
                surgeF = proportionalControl + derivativeControl
                self.previousSurgeForce = surgeF
                self.increment = surgeF/10
            else:
                if(abs(self.previousSurgeForce>self.increment)):
                    surgeF = self.previousSurgeForce - self.increment
                    self.previousSurgeForce = surgeF
                else:
                    surgeF = 0
                    self.previousSurgeForce = 0
        
        self.previousSurgeErr = self.surgeTrackingError
        return surgeF


class Tracker:
    def __init__(self, cnn):
        # CvBridge will be used to convert the image into a usable format for openCV
        self.bridge = CvBridge()
        self.image = None  # This is where the image will be stored
        # Variable used to chose if user wants to display the images or not (1 for yes)
        self.show = 1
        self.cnn = cnn #use cnn? 1 for yes
        self.iter = 0
        self.colourMap = {
            "Ball": ColourBounds((244,0,0))
        } #calculates the colour bounds to look for a red ball
        self.mvmt = Movement()
        self.reticuleR = 60
        self.mvmt.desiredR = self.reticuleR
        self.center = tuple()
        self.detected = False

    #camera callback function, attempting only every 5 frames to reduce load
    def onCamera(self, image):
        if(self.iter % 1 == 0):
            try:
                # converting the image into bgr format
                cv_image = self.bridge.imgmsg_to_cv2(image, "bgr8")
                self.image = cv_image  # Storing the image

                #Set goals for the tracking
                height = self.image.shape[0]
                width = self.image.shape[1]
                self.center = (width/2, height/2)
                self.mvmt.desiredX, self.mvmt.desiredY = self.center

                #Call detection
                self.noDeepLearning()
                windowName = "UCAT CAMERA Heuristic"
                self.showImage(self.image, windowName)
                self.mvmt.plot.x_values.append(self.iter)
                self.mvmt.plot.y_values.append(self.mvmt.Y-self.mvmt.desiredY)
            except CvBridgeError as e:
                print(e)

    def showImage(self, image, title):
        cv2.circle(image, self.center, self.reticuleR, (0, 0, 244), 2) #printing 'target reticule'
        if(image.all() != None and self.show):  # Make sure image is not empty and user want to display the image
	        # This displays the image in a new window
	        cv2.imshow(title, image)
        # If user presses "q" on keyboard, stop displaying the images
        if cv2.waitKey(1) & 0xFF == ord('q'):
            self.mvmt.plot.plot()
            self.show = 0
            cv2.destroyAllWindows() # This closes all openCV windows

    
    def drawLabel(self, x, y, r, text, frame):
        #print("Coordinates are", x, y,) #console message for coords
        #cv2.rectangle(frame,(x,y),(x+w,y+h),(120,0,0),2) #printing a rectangular bounding box 
        cv2.circle(frame, (x, y), r, (120,0,0),2) #printing circular bounding box
        cv2.putText(frame, text, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 2) #text for coords on screen
    
    #This method uses the hsv+vertice algorithm to check if there are round and red objects on the image and then bounds them
    def noDeepLearning(self):
        hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV) #converting image to hsv
        rects = {} #dict for detection data

        for name, colour in self.colourMap.items():
            #Check for area with the specified colour
            mask = cv2.inRange(hsv, colour.lower[0], colour.upper[0])

            if len(colour.lower) == 2:
                mask = mask | cv2.inRange(hsv, colour.lower[1], colour.upper[1])
            _, conts, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            if (len(conts) == 0): #if no areas found
                continue
            
            arranged = sorted(conts, key=cv2.contourArea, reverse=True)
            for cont in arranged: #goes through red contours checking shape
                perimeter = cv2.arcLength(cont, False) #takes perimeter of contour
                vertices = cv2.approxPolyDP(cont, 0.02*perimeter, True) #calculates vertices for contour
                if(len(vertices)>6): #if contour has more vertices than 6, must be circular, thus successful detections (can be increased for higher def images)
                    (x, y), r = cv2.minEnclosingCircle(cont)
                    self.mvmt.X, self.mvmt.Y = (x, y)
                    self.mvmt.radius = r
                    self.drawLabel(int(x), int(y), int(r), (str(int(x))+(',')+str(int(y))), self.image)
                    self.detected = True
                else:
                    self.detected = False
                    continue
    
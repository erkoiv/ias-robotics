#!/usr/bin/env python

"""
@author: Walid REMMAS
@contact: remmas.walid@gmail.com
          +33 6 62 55 45 04
"""

import rospy
import cv2 as cv2
from tracker import Tracker

import numpy as np
from math import sqrt
from cv_bridge import CvBridge, CvBridgeError
from nav_msgs.msg import Odometry
from geometry_msgs.msg import WrenchStamped, Pose, Vector3Stamped, Twist, Point, Vector3
from sensor_msgs.msg import Image, FluidPressure
from tut_arrows_msgs.msg import FlippersModeCmd, BeaconPing
from sensor_msgs.msg import Imu
from std_msgs.msg import String


class CameraBasedControl:
    def __init__(self, dt):

        self.dt = dt        # Sampling time

        '''
        STUDENT CODE
        '''

        #The next variables are in __init__ because they are used to retain information from previous loops and thus need to be pre-initialized
        self.lastDepthError = 0 #Recording error at iteration n-1
        self.previousForce = 0  #Made to slowly iterate down from force applied so that the forces aren't cancelled out when swimming in water
        self.increment = 0  #Increment for smooth flipper reset
        self.iterationNr = 0 #Counts iterations for performance evaluations, since ROS sequence counter is deprecated
        self.errSum = 0 #Sum of deptherrors for the RMSE calculations
        '''
        STUDENT CODE ^^^^^
        '''

        self.error = np.array([0, 0, 0])  # Tracking error at time t
        self.lastError = np.array([0, 0, 0])  # Tracking error at time t-1

        self.U = np.zeros(6)  # Force and Torque to be applied

        self.wrench_msg = WrenchStamped()  # Force and torque message
        self.wrench_pub = rospy.Publisher(
            "force_req", WrenchStamped, queue_size=10)  # Force and torque publisher

        self.mode = FlippersModeCmd()  # Mode selection
        self.mode.mode = "SLOW"  # Fins configuration ("FAST", "SLOW")
        self.wrench_mode_pub = rospy.Publisher(
            "force_mode", FlippersModeCmd, queue_size=25)  # Mode selection publisher

        self.rpy_sub = rospy.Subscriber("rpy", Vector3, self.imuCallback)

        '''
        STUDENT CODE
        '''
        self.pressure_sub = rospy.Subscriber(
            "/ucat0/hw/pressure", FluidPressure, self.depthCallback)
        self.ball_tracker = Tracker(0) #input 0 for non cnn, 1 if you have weights for tensorflow cnn algo
        self.camera_sub = rospy.Subscriber("/ucat0/camera/image", Image, self.ball_tracker.onCamera) # Subscriber to images topic
        '''
        STUDENT CODE ^^^^^
        '''

        self.depth = 0  # depth of robot
        self.roll = 0  # roll angle of robot
        self.pitch = 0  # pitch angle of robot
        self.yaw = 0  # yaw angle of robot

    # callback function of imu topic subscriber
    def imuCallback(self, imu_msg):
        self.roll = imu_msg.x
        self.pitch = imu_msg.y
        self.yaw = imu_msg.z

    def run(self):
        while not rospy.is_shutdown():
            self.step()
            rospy.sleep(self.dt)


    '''
    STUDENT CODE
    '''
    # Pressure to depth conversion function
    def depthCallback(self, pressure_msg):
        #print(pressure_msg)
        rho_g_z = pressure_msg.fluid_pressure - 101325
        self.depth = rho_g_z/(1000*9.81)
    '''
    STUDENT CODE ^^^^^
    '''

    # Main loop
    def step(self):
        e1 = self.error
        e2 = (self.error - self.lastError) / self.dt
        self.lastError = self.error
        
        '''
        STUDENT CODE
        '''
        self.iterationNr += 1
        self.ball_tracker.iter = self.iterationNr

        '''
        #RMSE calculation and printing every 200 iterations
        if(self.iterationNr % 200 == 0):
            timePassed = self.iterationNr*0.2
            self.errSum = self.errSum+depthTrackingError**2
            RMSE = sqrt((self.errSum)/timePassed)
            rospy.loginfo("After {}s, the RMSE of deviation from goal is {}".format(timePassed, RMSE))
            '''
        '''
        STUDENT CODE ^^^^^
        '''

        # Forces to apply : U = [surge, sway, heave, roll, pitch, yaw]
        # ===================================================================
        
        yawU = self.ball_tracker.mvmt.yaw(self.dt)
        depthU = self.ball_tracker.mvmt.depth(self.dt)
        surgeU = self.ball_tracker.mvmt.depth(self.dt)
        self.U = [surgeU, 0, depthU, 0, 0, yawU]
        if(self.iterationNr % 10 == 0):
            print("Changing depth with F={}".format(depthU))
            print("Changing distance with F={}".format(surgeU))
            print("Changing laterally with F={}".format(yawU))

        
        # ===================================================================

        # Publishing forces to wrench_driver
        self.wrench_msg.header.stamp = rospy.Time.now()
        self.wrench_msg.wrench.force.x = self.U[0]
        self.wrench_msg.wrench.force.y = self.U[1]
        self.wrench_msg.wrench.force.z = self.U[2]
        self.wrench_msg.wrench.torque.x = self.U[3]
        self.wrench_msg.wrench.torque.y = self.U[4]
        self.wrench_msg.wrench.torque.z = self.U[5]

        # Publishing force vector to fins wrench driver
        self.wrench_pub.publish(self.wrench_msg)


if __name__ == '__main__':

    rospy.init_node("CameraBasedControl")
    mbc = CameraBasedControl(0.2)  # (dt)
    mbc.run()

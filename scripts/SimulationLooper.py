#!/usr/bin/env python
# This script will start the robot simulation and run it until the set goal position is attained, then it will rerun the simulation with (hopefully) improved parameters

import roslaunch
import rospy
from std_msgs.msg import String

class looper():
    def __init__(self):

        self.topic = "/ucat0/restart"
        self.sub = rospy.Subscriber(self.topic, String, self.restartCallback)
        self.listener()

    def start(self):
        self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        roslaunch.configure_logging(self.uuid)
        self.launch = roslaunch.parent.ROSLaunchParent(
            self.uuid, ["/home/erik/catkin_ws/src/ucat_simulator-master/tut_arrows/launch/tracking_simulation.launch"])
        self.launch.start()
        rospy.loginfo("Started")
        self.sub = rospy.Subscriber(self.topic, String, self.restartCallback)

    def stop(self):
        self.sub.unregister()
        self.launch.shutdown()

    def restartCallback(self,message):
        if(message.data == "False"):
            rospy.sleep(0.2) #Run at same frequency as sim process
        else:
            self.stop()
            rospy.sleep(5)
            self.start()

    def listener(self):
        rospy.init_node('learning', anonymous=True)
        rospy.loginfo("Got to start")
        self.start()

        rospy.spin()

if __name__ == '__main__':
    loop = looper()